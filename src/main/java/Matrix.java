public class Matrix {

    public static int determinant(int[][] matrix) {
        int sum = 0;
        if (matrix.length == 1) return matrix[0][0];
        if (matrix.length == 2) {
            return matrix[0][0] * matrix[1][1] - matrix[1][0] * matrix[0][1];
        }
        int mult = 1;
        for (int i = 0; i < matrix.length; i++) {
            sum += mult * matrix[0][i] * determinant(smallDeterminant(matrix, i));
            mult *= -1;
        }
        return sum;
    }

    public static int[][] smallDeterminant(int[][] matrix, int determinantPosition) {
        int smallDeterminant[][] = new int[matrix.length - 1][matrix.length - 1];
        for (int i = 1; i < matrix.length; i++) {
            for (int j = 0; j < matrix.length; j++) {
                if (j != determinantPosition) {
                    smallDeterminant[i - 1][j - (determinantPosition < j ? 1 : 0)] = matrix[i][j];
                }
            }
        }
        return smallDeterminant;
    }

}
