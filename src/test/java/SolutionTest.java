import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class SolutionTest {

    private static int[][][] matrix = {{{1}},
            {{1, 3}, {2, 5}},
            {{2, 5, 3}, {1, -2, -1}, {1, 3, 4}}};

    private static int[] expected = {1, -1, -20};

    private static String[] msg = {"Determinant of a 1 x 1 matrix yields the value of the one element",
            "Should return 1 * 5 - 3 * 2 == -1 ",
            ""};

    private static int[][] bigDeterminant = {{1, 2, 3}, {4, 5, 6}, {7, 8, 9}};
    private static int[][] smallDeterminant = {{5, 6}, {8, 9}};
    private static int[][] smallDeterminant2 = {{4, 6}, {7, 9}};


    @Test
    public void sampleTests() {
        for (int n = 0; n < expected.length; n++)
            assertEquals(msg[n], expected[n], Matrix.determinant(matrix[n]));
    }

    @Test
    public void smallDeterminantTest() {
        assertEquals(Matrix.smallDeterminant(bigDeterminant, 0), smallDeterminant);
        assertEquals(Matrix.smallDeterminant(bigDeterminant, 1), smallDeterminant2);

    }


}